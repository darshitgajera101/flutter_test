class Assets {
  // static String iconsPath = "assets/icons";
  static String imagesPath = "assets/images";

  static String agreement = "$imagesPath/agreement.svg";
  static String tabScreen1_1 = "$imagesPath/tab_screen_1_1.svg";
  static String tabScreen1_2 = "$imagesPath/tab_screen_1_2.svg";
  static String tabScreen1_3 = "$imagesPath/tab_screen_1_3.svg";
  static String tabScreen2_2 = "$imagesPath/tab_screen_2_2.svg";
  static String tabScreen2_3 = "$imagesPath/tab_screen_2_3.svg";
  static String tabScreen3_2 = "$imagesPath/tab_screen_3_2.svg";
  static String tabScreen3_3 = "$imagesPath/tab_screen_3_3.svg";
  static String arrow1 = "$imagesPath/arrow1.svg";
  static String arrow2 = "$imagesPath/arrow2.svg";
}
