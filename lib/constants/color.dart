import 'package:flutter/material.dart';

const Color kColorBlack = Color(0xFF2D3748);
const Color kColorLightBlack = Color(0xFF4A5568);
const Color kColorBlackGrey = Color(0xFF718096);

const Color kColorLightCyan = Color(0xFFE6FFFA);
const Color kColorLightCyan2 = Color(0xFF81E6D9);
const Color kColorCyan = Color(0xFF319795);
const Color kColorLightBlue = Color(0xFF3182CE);
const Color kColorDarkBlue = Color(0xFF1E1C44);


const Color kColorGrey = Color(0xFFCBD5E0);
