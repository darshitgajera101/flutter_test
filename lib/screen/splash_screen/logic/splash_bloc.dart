import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:web_demo/screen/splash_screen/logic/splash_event.dart';
import 'package:web_demo/screen/splash_screen/logic/splash_state.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  SplashBloc() : super(SplashState()) {
    on<SplashInitialEvent>(_splashInitialEvent);
  }

  Future<void> _splashInitialEvent(
      SplashInitialEvent event, Emitter<SplashState> emit) async {
    await Future.delayed(const Duration(seconds: 2)).then((value) {
      emit(NavigateToHomeScreenState());
    });
  }
}
