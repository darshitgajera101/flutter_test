class SplashState {}

class SplashInitialState extends SplashState {
  @override
  String toString() {
    return 'SplashInitialState';
  }
}

class NavigateToHomeScreenState extends SplashState {
  @override
  String toString() {
    return 'NavigateToHomeScreenState';
  }
}
