import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:web_demo/constants/color.dart';
import 'package:web_demo/screen/splash_screen/logic/splash_bloc.dart';
import 'package:web_demo/screen/splash_screen/logic/splash_event.dart';
import 'package:web_demo/screen/splash_screen/logic/splash_state.dart';
import 'package:web_demo/screen/tab_navigation_screen/view/tab_navigation_screen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SplashBloc>(
      create: (context) => SplashBloc()..add(SplashInitialEvent()),
      child: const _SplashScreen(),
    );
  }
}

class _SplashScreen extends StatefulWidget {
  const _SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<_SplashScreen> {
  late final SplashBloc splashBloc;

  @override
  void initState() {
    splashBloc = BlocProvider.of<SplashBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashBloc, SplashState>(
      bloc: splashBloc,
      listener: (BuildContext context, SplashState state) {
        if (state is NavigateToHomeScreenState) {
          Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) => const TabNavigationScreen()),
            (Route<dynamic> route) => false,
          );
        }
      },
      child: SafeArea(
        child: Scaffold(
          body: Container(
            color: kColorDarkBlue,
            child: const Center(
              child: Text(
                'Flutter \nTest \nIndex',
                style: TextStyle(
                  color: kColorCyan,
                  fontSize: 75,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
