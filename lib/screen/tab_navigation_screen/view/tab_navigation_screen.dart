import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:web_demo/constants/assets.dart';
import 'package:web_demo/constants/color.dart';
import 'package:web_demo/screen/tab_navigation_screen/view_model/tab_navigation_bloc.dart';
import 'package:web_demo/screen/tab_navigation_screen/view_model/tab_navigation_event.dart';
import 'package:web_demo/screen/tab_navigation_screen/view_model/tab_navigation_state.dart';
import 'package:web_demo/shared_widget/custom_button.dart';
import 'package:web_demo/shared_widget/tab_tile.dart';
import 'package:web_demo/shared_widget/tab_widget.dart';

class TabNavigationScreen extends StatelessWidget {
  const TabNavigationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TabNavigationBloc>(
      create: (context) {
        return TabNavigationBloc()..add(TabNavigationInitialEvent());
      },
      child: const _TabNavigationScreen(),
    );
  }
}

class _TabNavigationScreen extends StatefulWidget {
  const _TabNavigationScreen({Key? key}) : super(key: key);

  @override
  _TabNavigationScreenState createState() => _TabNavigationScreenState();
}

class _TabNavigationScreenState extends State<_TabNavigationScreen> {
  late final TabNavigationBloc tabNavigationBloc;

  ScrollController scrollController = ScrollController();
  double scrollPosition = 0;

  @override
  void initState() {
    tabNavigationBloc = BlocProvider.of<TabNavigationBloc>(context);
    scrollController.addListener(() {
      setState(() {
        scrollPosition = scrollController.position.pixels;
        debugPrint(scrollPosition.toString());
      });

      // emit(TabNavigationInitialState());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    return BlocListener<TabNavigationBloc, TabNavigationState>(
      bloc: tabNavigationBloc,
      listener: (BuildContext context, TabNavigationState state) {},
      child: BlocBuilder<TabNavigationBloc, TabNavigationState>(
        buildWhen: (previous, current) {
          if (current is TabNavigationInitialState) {
            return true;
          }
          return false;
        },
        bloc: tabNavigationBloc,
        builder: (BuildContext context, TabNavigationState state) {
          return Scaffold(
            backgroundColor: kColorLightCyan,
            body: SafeArea(
              child: Stack(
                children: [
                  Positioned.fill(
                    child: SingleChildScrollView(
                      controller: scrollController,
                      physics: const BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 100,
                          ),
                          deviceWidth > 500
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const SizedBox(
                                      height: 70,
                                    ),
                                    Wrap(
                                      crossAxisAlignment:
                                          WrapCrossAlignment.center,
                                      children: [
                                        Column(
                                          children: [
                                            const Text(
                                              'Deine Job \nwebsite',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize: 65,
                                                  fontWeight: FontWeight.w600,
                                                  color: kColorBlack),
                                            ),
                                            CustomButton(
                                              title: 'Kostenlos Registrieren',
                                              onTap: () {},
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          width: 100,
                                        ),
                                        ClipOval(
                                          child: SizedBox.square(
                                            child: Container(
                                              color: Colors.white,
                                              child: SvgPicture.asset(
                                                Assets.agreement,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 70,
                                    ),
                                  ],
                                )
                              : Column(
                                  children: [
                                    const Text(
                                      'Deine Job \nwebsite',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 42,
                                          fontWeight: FontWeight.w600,
                                          color: kColorBlack),
                                    ),
                                    SvgPicture.asset(
                                      Assets.agreement,
                                    ),
                                  ],
                                ),
                          Container(
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(12),
                                topRight: Radius.circular(12),
                              ),
                            ),
                            child: BlocBuilder<TabNavigationBloc,
                                    TabNavigationState>(
                                buildWhen: (previous, current) {
                                  if (current is TabNavigationRefreshState) {
                                    return true;
                                  }
                                  return false;
                                },
                                bloc: tabNavigationBloc,
                                builder: (BuildContext context,
                                    TabNavigationState state) {
                                  return Column(
                                    children: [
                                      if (deviceWidth <= 500)
                                        CustomButton(
                                          title: 'Kostenlos Registrieren',
                                          onTap: () {},
                                        ),
                                      const SizedBox(
                                        height: 58,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(20),
                                        child: Container(
                                          constraints: const BoxConstraints(
                                              maxWidth: 500),
                                          child: Row(
                                            children: [
                                              TabTile(
                                                onTap: () {
                                                  tabNavigationBloc.add(
                                                      TabNavigationChangeEvent(
                                                          index: 0));
                                                },
                                                title: 'Arbeitnehmer',
                                                isSelected: tabNavigationBloc
                                                        .selectedTab ==
                                                    0,
                                                isFirst: true,
                                              ),
                                              TabTile(
                                                onTap: () {
                                                  tabNavigationBloc.add(
                                                      TabNavigationChangeEvent(
                                                          index: 1));
                                                },
                                                title: 'Arbeitgeber',
                                                isSelected: tabNavigationBloc
                                                        .selectedTab ==
                                                    1,
                                              ),
                                              TabTile(
                                                onTap: () {
                                                  tabNavigationBloc.add(
                                                      TabNavigationChangeEvent(
                                                          index: 2));
                                                },
                                                title: 'Temporärbüro',
                                                isSelected: tabNavigationBloc
                                                        .selectedTab ==
                                                    2,
                                                isLast: true,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      if (tabNavigationBloc.selectedTab == 0)
                                        TabWidget(
                                          title:
                                              'Drei einfache Schritte \nzu deinem neuen Job',
                                          step1Title:
                                              'Erstellen dein Lebenslauf',
                                          step2Title:
                                              'Erstellen dein Lebenslauf',
                                          step3Title: deviceWidth > 500
                                              ? 'Mit nur einem Klick bewerben'
                                              : 'Mit nur einem Klick \nbewerben',
                                          step1Image: Assets.tabScreen1_1,
                                          step2Image: Assets.tabScreen1_2,
                                          step3Image: Assets.tabScreen1_3,
                                        ),
                                      if (tabNavigationBloc.selectedTab == 1)
                                        TabWidget(
                                          title:
                                              'Drei einfache Schritte \nzu deinem neuen Mitarbeiter',
                                          step1Title: deviceWidth > 500
                                              ? 'Erstellen dein Unternehmensprofil'
                                              : 'Erstellen dein \nUnternehmensprofil',
                                          step2Title:
                                              'Erstellen ein Jobinserat',
                                          step3Title: deviceWidth > 500
                                              ? 'Wähle deinen neuen Mitarbeiter aus'
                                              : 'Wähle deinen \nneuen Mitarbeiter aus',
                                          step1Image: Assets.tabScreen1_1,
                                          step2Image: Assets.tabScreen2_2,
                                          step3Image: Assets.tabScreen2_3,
                                        ),
                                      if (tabNavigationBloc.selectedTab == 2)
                                        TabWidget(
                                          title:
                                              'Drei einfache Schritte zur \nVermittlung neuer Mitarbeiter',
                                          step1Title: deviceWidth > 500
                                              ? 'Erstellen dein Unternehmensprofil'
                                              : 'Erstellen dein \nUnternehmensprofil',
                                          step2Title: deviceWidth > 500
                                              ? 'Erhalte Vermittlungs-angebot von Arbeitgeber'
                                              : 'Erhalte Vermittlungs-\nangebot von Arbeitgeber',
                                          step3Title: deviceWidth > 500
                                              ? 'Vermittlung nach Provision oder Stundenlohn'
                                              : 'Vermittlung nach \nProvision oder \nStundenlohn',
                                          step1Image: Assets.tabScreen1_1,
                                          step2Image: Assets.tabScreen3_2,
                                          step3Image: Assets.tabScreen3_3,
                                        ),
                                    ],
                                  );
                                }),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                      height: 80,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(12),
                          bottomRight: Radius.circular(12),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 10,
                            spreadRadius: 3,
                          ),
                        ],
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            width: double.infinity,
                            height: 5,
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  kColorCyan,
                                  kColorLightBlue,
                                ],
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 75,
                              ),
                              if (deviceWidth > 500)
                                if (scrollPosition > 400)
                                  Expanded(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        const Text(
                                          'Jetzt Klicken',
                                          style: TextStyle(
                                            color: kColorLightBlack,
                                            fontSize: 19,
                                          ),
                                        ),

                                        // AnimatedPositioned(
                                        //     child:
                                        Flexible(
                                          child: CustomButton(
                                            title: 'Kostenlos Registrieren',
                                            isSimple: true,
                                            onTap: () {},
                                          ),
                                        ),
                                        // bottom: 200,
                                        // left: 200,
                                        // duration: const Duration(seconds: 3)),
                                      ],
                                    ),
                                  ),
                              TextButton(
                                onPressed: () {},
                                child: const Text(
                                  'Login',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: kColorCyan,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
