import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:web_demo/screen/tab_navigation_screen/view_model/tab_navigation_event.dart';
import 'package:web_demo/screen/tab_navigation_screen/view_model/tab_navigation_state.dart';

class TabNavigationBloc extends Bloc<TabNavigationEvent, TabNavigationState> {
  TabNavigationBloc() : super(TabNavigationState()) {
    on<TabNavigationInitialEvent>(_tabNavigationInitialEvent);
    on<TabNavigationChangeEvent>(_tabNavigationChangeEvent);
  }
  int selectedTab = 0;

  Future<void> _tabNavigationInitialEvent(TabNavigationInitialEvent event,
      Emitter<TabNavigationState> emit) async {}

  Future<void> _tabNavigationChangeEvent(
      TabNavigationChangeEvent event, Emitter<TabNavigationState> emit) async {
    selectedTab = event.index;
    emit(TabNavigationRefreshState());
  }
}
