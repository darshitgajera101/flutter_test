class TabNavigationState {}

class TabNavigationInitialState extends TabNavigationState {
  @override
  String toString() {
    return 'TabNavigationInitialState';
  }
}

class TabNavigationRefreshState extends TabNavigationState {
  @override
  String toString() {
    return 'TabNavigationRefreshState';
  }
}
