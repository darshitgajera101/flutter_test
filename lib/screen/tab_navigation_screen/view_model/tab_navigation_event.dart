class TabNavigationEvent {}

class TabNavigationInitialEvent extends TabNavigationEvent {
  @override
  String toString() {
    return 'TabNavigationInitialEvent';
  }
}

class TabNavigationChangeEvent extends TabNavigationEvent {
  TabNavigationChangeEvent({required this.index});

  final int index;

  @override
  String toString() {
    return 'TabNavigationChangeEvent';
  }
}
