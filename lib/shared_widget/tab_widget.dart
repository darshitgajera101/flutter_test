import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:web_demo/constants/assets.dart';
import 'package:web_demo/constants/color.dart';

class TabWidget extends StatelessWidget {
  const TabWidget({
    Key? key,
    required this.title,
    required this.step1Title,
    required this.step2Title,
    required this.step3Title,
    required this.step1Image,
    required this.step2Image,
    required this.step3Image,
  }) : super(key: key);
  final String title;
  final String step1Title;
  final String step1Image;
  final String step2Title;
  final String step2Image;
  final String step3Title;
  final String step3Image;

  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;

    if (deviceWidth > 500) {
      return Column(
        children: [
          Text(
            title,
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: kColorLightBlack,
              fontSize: 40,
            ),
          ),
          const SizedBox(
            height: 80,
          ),
          Stack(
            children: [
              Positioned(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Expanded(flex: 1, child: SizedBox()),
                    Expanded(
                      flex: 3,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            const Text(
                              '1.',
                              style: TextStyle(
                                color: kColorBlackGrey,
                                fontSize: 120,
                              ),
                            ),
                            if (deviceWidth > 700)
                              Flexible(
                                child: Padding(
                                  padding: const EdgeInsets.only(bottom: 20),
                                  child: Text(
                                    step1Title,
                                    style: const TextStyle(
                                      color: kColorBlackGrey,
                                      fontSize: 30,
                                    ),
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                    SvgPicture.asset(
                      step1Image,
                    ),
                    const Expanded(flex: 2, child: SizedBox()),
                  ],
                ),
              ),
              if (deviceWidth > 800)
                Positioned(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 150),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Expanded(flex: 1, child: SizedBox()),
                        const SizedBox(
                          width: 30,
                        ),
                        Expanded(
                          // flex: 1,
                          child: SvgPicture.asset(
                            Assets.arrow1,
                            height: 150,
                            width: 380,
                            fit: BoxFit.fill,
                          ),
                        ),
                        const Expanded(flex: 2, child: SizedBox()),
                      ],
                    ),
                  ),
                ),
              Positioned(
                child: Padding(
                  padding: const EdgeInsets.only(top: 300),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const Expanded(flex: 2, child: SizedBox()),
                      SvgPicture.asset(
                        step2Image,
                      ),
                      Expanded(
                        flex: 3,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 40),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              const Text(
                                '2.',
                                style: TextStyle(
                                  color: kColorBlackGrey,
                                  fontSize: 120,
                                ),
                              ),
                              if (deviceWidth > 700)
                                Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 20),
                                    child: Text(
                                      step2Title,
                                      style: const TextStyle(
                                        color: kColorBlackGrey,
                                        fontSize: 30,
                                      ),
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ),
                      const Expanded(flex: 1, child: SizedBox()),
                    ],
                  ),
                ),
              ),
              if (deviceWidth > 800)
                Positioned(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 450),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Expanded(flex: 1, child: SizedBox()),
                        SizedBox(
                          width: deviceWidth > 1080 ? 150 : 50,
                        ),
                        Expanded(
                          // flex: 1,
                          child: SvgPicture.asset(
                            Assets.arrow2,
                            height: 150,
                            width: 380,
                            fit: BoxFit.fill,
                          ),
                        ),
                        const Expanded(flex: 2, child: SizedBox()),
                      ],
                    ),
                  ),
                ),
              Positioned(
                child: Padding(
                  padding: const EdgeInsets.only(top: 600),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const Expanded(flex: 2, child: SizedBox()),
                      Expanded(
                        flex: 3,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 40),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              const Text(
                                '3.',
                                style: TextStyle(
                                  color: kColorBlackGrey,
                                  fontSize: 120,
                                ),
                              ),
                              if (deviceWidth > 700)
                                Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 20),
                                    child: Text(
                                      step3Title,
                                      style: const TextStyle(
                                        color: kColorBlackGrey,
                                        fontSize: 30,
                                      ),
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: SvgPicture.asset(
                          step3Image,
                        ),
                      ),
                      const Expanded(flex: 1, child: SizedBox()),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      );
      // return Column(
      //   children: [
      //     Text(
      //       title,
      //       textAlign: TextAlign.center,
      //       style: const TextStyle(
      //         color: kColorLightBlack,
      //         fontSize: 40,
      //       ),
      //     ),
      //     const SizedBox(
      //       height: 80,
      //     ),
      //     Row(
      //       mainAxisAlignment: MainAxisAlignment.start,
      //       children: [
      //         if (deviceWidth > 1080)
      //           const Expanded(flex: 1, child: SizedBox()),
      //         Flexible(
      //           child: Padding(
      //             padding: const EdgeInsets.only(right: 40),
      //             child: Text.rich(
      //               TextSpan(
      //                 text: '1.',
      //                 style: const TextStyle(
      //                   color: kColorBlackGrey,
      //                   fontSize: 130,
      //                 ),
      //                 children: <InlineSpan>[
      //                   TextSpan(
      //                     text: step1Title,
      //                     style: const TextStyle(
      //                       color: kColorBlackGrey,
      //                       fontSize: 30,
      //                     ),
      //                   )
      //                 ],
      //               ),
      //             ),
      //           ),
      //         ),
      //         SvgPicture.asset(
      //           step1Image,
      //         ),
      //         if (deviceWidth > 1080)
      //           const Expanded(flex: 2, child: SizedBox()),
      //       ],
      //     ),
      //     const SizedBox(
      //       height: 150,
      //     ),
      //     Row(
      //       mainAxisAlignment: MainAxisAlignment.end,
      //       children: [
      //         if (deviceWidth > 1080)
      //           const Expanded(flex: 2, child: SizedBox()),
      //         SvgPicture.asset(
      //           step2Image,
      //         ),
      //         Flexible(
      //           child: Padding(
      //             padding: const EdgeInsets.only(left: 40),
      //             child: Text.rich(
      //               TextSpan(
      //                 text: '2.',
      //                 style: const TextStyle(
      //                   color: kColorBlackGrey,
      //                   fontSize: 130,
      //                 ),
      //                 children: <InlineSpan>[
      //                   TextSpan(
      //                     text: step2Title,
      //                     style: const TextStyle(
      //                       color: kColorBlackGrey,
      //                       fontSize: 30,
      //                     ),
      //                   )
      //                 ],
      //               ),
      //             ),
      //           ),
      //         ),
      //         if (deviceWidth > 1080)
      //           const Expanded(flex: 1, child: SizedBox()),
      //       ],
      //     ),
      //     const SizedBox(
      //       height: 150,
      //     ),
      //     Row(
      //       mainAxisAlignment: MainAxisAlignment.center,
      //       children: [
      //         Flexible(
      //           child: Padding(
      //             padding: const EdgeInsets.only(left: 40),
      //             child: Text.rich(
      //               TextSpan(
      //                 text: '3.',
      //                 style: const TextStyle(
      //                   color: kColorBlackGrey,
      //                   fontSize: 130,
      //                 ),
      //                 children: <InlineSpan>[
      //                   TextSpan(
      //                     text: step3Title,
      //                     style: const TextStyle(
      //                       color: kColorBlackGrey,
      //                       fontSize: 30,
      //                     ),
      //                   )
      //                 ],
      //               ),
      //             ),
      //           ),
      //         ),
      //         SvgPicture.asset(
      //           step3Image,
      //         ),
      //       ],
      //     ),
      //     const SizedBox(
      //       height: 140,
      //     ),
      //   ],
      // );
    } else {
      return Column(
        children: [
          Text(
            title,
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: kColorLightBlack,
              fontSize: 21,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.only(right: 40),
              child: SvgPicture.asset(
                step1Image,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 40),
            child: Text.rich(
              TextSpan(
                text: '1.',
                style: const TextStyle(
                  color: kColorBlackGrey,
                  fontSize: 130,
                ),
                children: <InlineSpan>[
                  TextSpan(
                    text: step1Title,
                    style: const TextStyle(
                      color: kColorBlackGrey,
                      fontSize: 15.75,
                    ),
                  )
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40),
            child: Text.rich(
              TextSpan(
                text: '2.',
                style: const TextStyle(
                  color: kColorBlackGrey,
                  fontSize: 130,
                ),
                children: <InlineSpan>[
                  TextSpan(
                    text: step2Title,
                    style: const TextStyle(
                      color: kColorBlackGrey,
                      fontSize: 15.75,
                    ),
                  )
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.only(right: 80),
              child: SvgPicture.asset(
                step2Image,
              ),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text(
                '3.',
                style: TextStyle(
                  color: kColorBlackGrey,
                  fontSize: 130,
                ),
              ),
              Text(
                step3Title,
                style: const TextStyle(
                  color: kColorBlackGrey,
                  fontSize: 15.75,
                ),
              ),
              const SizedBox(
                width: 35,
              ),
            ],
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.only(right: 40),
              child: SvgPicture.asset(
                step3Image,
              ),
            ),
          ),
          const SizedBox(
            height: 140,
          ),
        ],
      );
    }
  }
}
