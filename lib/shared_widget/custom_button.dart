import 'package:flutter/material.dart';
import 'package:web_demo/constants/color.dart';

class CustomButton extends StatelessWidget {
  const CustomButton(
      {Key? key,
      required this.title,
      this.isSimple = false,
      required this.onTap})
      : super(key: key);

  final String title;
  final bool isSimple;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        constraints: const BoxConstraints(maxWidth: 400),
        margin: const EdgeInsets.all(17),
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          border: isSimple ? Border.all(color: kColorGrey, width: 0.5) : null,
          gradient: isSimple
              ? null
              : const LinearGradient(
                  colors: [
                    kColorCyan,
                    kColorLightBlue,
                  ],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                ),
        ),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
              color: isSimple ? kColorCyan : Colors.white,
              fontSize: 14,
            ),
          ),
        ),
      ),
    );
  }
}
