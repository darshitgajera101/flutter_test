import 'package:flutter/material.dart';
import 'package:web_demo/constants/color.dart';

class TabTile extends StatelessWidget {
  const TabTile({
    Key? key,
    required this.title,
    required this.isSelected,
    required this.onTap,
    this.isFirst = false,
    this.isLast = false,
  }) : super(key: key);

  final String title;
  final bool isSelected;
  final Function() onTap;
  final bool? isFirst;
  final bool? isLast;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.all(12),
          decoration: BoxDecoration(
            color: isSelected ? kColorLightCyan2 : null,
            border: Border.all(color: kColorGrey, width: 0.5),
            borderRadius: isFirst!
                ? const BorderRadius.only(
                    topLeft: Radius.circular(12),
                    bottomLeft: Radius.circular(12),
                  )
                : isLast!
                    ? const BorderRadius.only(
                        topRight: Radius.circular(12),
                        bottomRight: Radius.circular(12),
                      )
                    : null,
          ),
          child: Center(
            child: Text(
              title,
              style: TextStyle(
                  color: isSelected ? Colors.white : kColorLightCyan2),
            ),
          ),
        ),
      ),
    );
  }
}
